#pragma once
#include <iostream>
#include <ctime>
#include <conio.h>
#include <windows.h>
#include <MMSystem.h>

using namespace std;
namespace graphicsexample {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form 
	{
	public:
		int dice;
		int u1Pos, u2Pos;
		int u1Sart;
		int u2Start;
		int u;
		int sixCount;
		int key;
		int x,y;
		int g;
		bool Single;
		bool doubleplayer;
		
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::PictureBox^  pictureBox5;
	private: System::Windows::Forms::PictureBox^  pictureBox6;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::PictureBox^  pictureBox_DiceSix;
	private: System::Windows::Forms::PictureBox^  pictureBox_DiceFive;
	private: System::Windows::Forms::PictureBox^  pictureBox_DiceFour;
	private: System::Windows::Forms::PictureBox^  pictureBox_DiceThree;
	private: System::Windows::Forms::PictureBox^  pictureBox_DiceTwo;
	private: System::Windows::Forms::PictureBox^  pictureBox_DiceOne;
	private: System::Windows::Forms::PictureBox^  pictureBox7;
	private: System::Windows::Forms::PictureBox^  pictureBox8;
	private: System::Windows::Forms::PictureBox^  pictureBox9;
	private: System::Windows::Forms::PictureBox^  pictureBox10;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: Microsoft::VisualBasic::PowerPacks::ShapeContainer^  shapeContainer1;
	private: Microsoft::VisualBasic::PowerPacks::RectangleShape^  rectangleShape1;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::PictureBox^  pictureBox11;
	private: System::Windows::Forms::RichTextBox^  richTextBox1;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  singlePlayerToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  singlePlayerToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  doublePlayerToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  doublePlayerToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  singlePlayerToolStripMenuItem2;
	private: System::Windows::Forms::ToolStripMenuItem^  doublePlayerToolStripMenuItem2;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::RichTextBox^  richTextBox2;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	public: 
	private: System::Windows::Forms::Label^  label3;
	public: 
		//int sixCount;
    
		Form1(void)
		{
			InitializeComponent();
			dice=0;
			u1Pos=0, u2Pos=0;
			u1Sart=0;
			u2Start=0;
			u=1;
			sixCount=0;
			key=0;
			x=pictureBox2->Location.X;
			y=pictureBox2->Location.Y;
			g=0;
			button1->Visible = false;
			doubleplayer=false;
			Single=false;
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	protected: 
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
private: System::ComponentModel::IContainer^  components;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox5 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox6 = (gcnew System::Windows::Forms::PictureBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->pictureBox_DiceSix = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox_DiceFive = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox_DiceFour = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox_DiceThree = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox_DiceTwo = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox_DiceOne = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox7 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox8 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox9 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox10 = (gcnew System::Windows::Forms::PictureBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->shapeContainer1 = (gcnew Microsoft::VisualBasic::PowerPacks::ShapeContainer());
			this->rectangleShape1 = (gcnew Microsoft::VisualBasic::PowerPacks::RectangleShape());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->pictureBox11 = (gcnew System::Windows::Forms::PictureBox());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->singlePlayerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->singlePlayerToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->doublePlayerToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->doublePlayerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->singlePlayerToolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->doublePlayerToolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->richTextBox2 = (gcnew System::Windows::Forms::RichTextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceSix))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceFive))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceFour))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceThree))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceTwo))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceOne))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox10))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox11))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 14.25F, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(861, 597);
			this->button1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(201, 86);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Roll Dice";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::SystemColors::Control;
			this->label1->Location = System::Drawing::Point(929, 458);
			this->label1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(90, 17);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Dice Not Roll";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(927, 62);
			this->label2->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(34, 29);
			this->label2->TabIndex = 2;
			this->label2->Text = L" 1";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->ImageLocation = L"";
			this->pictureBox1->InitialImage = nullptr;
			this->pictureBox1->Location = System::Drawing::Point(16, 62);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(600, 600);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox1->TabIndex = 3;
			this->pictureBox1->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->Location = System::Drawing::Point(43, 738);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(36, 35);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox2->TabIndex = 4;
			this->pictureBox2->TabStop = false;
			this->pictureBox2->Visible = false;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(831, 62);
			this->label3->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(93, 31);
			this->label3->TabIndex = 6;
			this->label3->Text = L"User :";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label4->Font = (gcnew System::Drawing::Font(L"Arial", 11.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(1017, 122);
			this->label4->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(21, 22);
			this->label4->TabIndex = 7;
			this->label4->Text = L"0";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label5->Font = (gcnew System::Drawing::Font(L"Arial", 11.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(1020, 180);
			this->label5->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(21, 22);
			this->label5->TabIndex = 8;
			this->label5->Text = L"0";
			// 
			// pictureBox3
			// 
			this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox3.Image")));
			this->pictureBox3->Location = System::Drawing::Point(44, 738);
			this->pictureBox3->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(35, 35);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox3->TabIndex = 9;
			this->pictureBox3->TabStop = false;
			this->pictureBox3->Visible = false;
			// 
			// pictureBox4
			// 
			this->pictureBox4->Location = System::Drawing::Point(-957, 225);
			this->pictureBox4->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(405, 326);
			this->pictureBox4->TabIndex = 10;
			this->pictureBox4->TabStop = false;
			this->pictureBox4->Visible = false;
			// 
			// pictureBox5
			// 
			this->pictureBox5->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox5.Image")));
			this->pictureBox5->Location = System::Drawing::Point(-1, 64);
			this->pictureBox5->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox5->Name = L"pictureBox5";
			this->pictureBox5->Size = System::Drawing::Size(832, 762);
			this->pictureBox5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
			this->pictureBox5->TabIndex = 11;
			this->pictureBox5->TabStop = false;
			this->pictureBox5->Visible = false;
			// 
			// pictureBox6
			// 
			this->pictureBox6->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox6.Image")));
			this->pictureBox6->Location = System::Drawing::Point(-1, 63);
			this->pictureBox6->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox6->Name = L"pictureBox6";
			this->pictureBox6->Size = System::Drawing::Size(832, 775);
			this->pictureBox6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
			this->pictureBox6->TabIndex = 12;
			this->pictureBox6->TabStop = false;
			this->pictureBox6->Visible = false;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label6->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(832, 123);
			this->label6->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(161, 24);
			this->label6->TabIndex = 13;
			this->label6->Text = L"Position of User 1:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label7->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->label7->Location = System::Drawing::Point(832, 180);
			this->label7->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(161, 24);
			this->label7->TabIndex = 14;
			this->label7->Text = L"Position of User 2:";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label8->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->label8->Location = System::Drawing::Point(839, 452);
			this->label8->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(0, 24);
			this->label8->TabIndex = 15;
			// 
			// pictureBox_DiceSix
			// 
			this->pictureBox_DiceSix->BackColor = System::Drawing::SystemColors::Control;
			this->pictureBox_DiceSix->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox_DiceSix->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox_DiceSix.Image")));
			this->pictureBox_DiceSix->Location = System::Drawing::Point(927, 449);
			this->pictureBox_DiceSix->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox_DiceSix->Name = L"pictureBox_DiceSix";
			this->pictureBox_DiceSix->Size = System::Drawing::Size(120, 114);
			this->pictureBox_DiceSix->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox_DiceSix->TabIndex = 16;
			this->pictureBox_DiceSix->TabStop = false;
			this->pictureBox_DiceSix->Visible = false;
			// 
			// pictureBox_DiceFive
			// 
			this->pictureBox_DiceFive->BackColor = System::Drawing::SystemColors::Control;
			this->pictureBox_DiceFive->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox_DiceFive->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox_DiceFive.Image")));
			this->pictureBox_DiceFive->Location = System::Drawing::Point(925, 449);
			this->pictureBox_DiceFive->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox_DiceFive->Name = L"pictureBox_DiceFive";
			this->pictureBox_DiceFive->Size = System::Drawing::Size(120, 114);
			this->pictureBox_DiceFive->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox_DiceFive->TabIndex = 17;
			this->pictureBox_DiceFive->TabStop = false;
			this->pictureBox_DiceFive->Visible = false;
			// 
			// pictureBox_DiceFour
			// 
			this->pictureBox_DiceFour->BackColor = System::Drawing::SystemColors::Control;
			this->pictureBox_DiceFour->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox_DiceFour->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox_DiceFour.Image")));
			this->pictureBox_DiceFour->Location = System::Drawing::Point(927, 452);
			this->pictureBox_DiceFour->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox_DiceFour->Name = L"pictureBox_DiceFour";
			this->pictureBox_DiceFour->Size = System::Drawing::Size(120, 114);
			this->pictureBox_DiceFour->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox_DiceFour->TabIndex = 18;
			this->pictureBox_DiceFour->TabStop = false;
			this->pictureBox_DiceFour->Visible = false;
			// 
			// pictureBox_DiceThree
			// 
			this->pictureBox_DiceThree->BackColor = System::Drawing::SystemColors::Control;
			this->pictureBox_DiceThree->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox_DiceThree->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox_DiceThree.Image")));
			this->pictureBox_DiceThree->Location = System::Drawing::Point(927, 450);
			this->pictureBox_DiceThree->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox_DiceThree->Name = L"pictureBox_DiceThree";
			this->pictureBox_DiceThree->Size = System::Drawing::Size(120, 114);
			this->pictureBox_DiceThree->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox_DiceThree->TabIndex = 19;
			this->pictureBox_DiceThree->TabStop = false;
			this->pictureBox_DiceThree->Visible = false;
			// 
			// pictureBox_DiceTwo
			// 
			this->pictureBox_DiceTwo->BackColor = System::Drawing::SystemColors::Control;
			this->pictureBox_DiceTwo->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox_DiceTwo->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox_DiceTwo.Image")));
			this->pictureBox_DiceTwo->Location = System::Drawing::Point(925, 450);
			this->pictureBox_DiceTwo->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox_DiceTwo->Name = L"pictureBox_DiceTwo";
			this->pictureBox_DiceTwo->Size = System::Drawing::Size(120, 114);
			this->pictureBox_DiceTwo->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox_DiceTwo->TabIndex = 20;
			this->pictureBox_DiceTwo->TabStop = false;
			this->pictureBox_DiceTwo->Visible = false;
			// 
			// pictureBox_DiceOne
			// 
			this->pictureBox_DiceOne->BackColor = System::Drawing::SystemColors::Control;
			this->pictureBox_DiceOne->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox_DiceOne->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox_DiceOne.Image")));
			this->pictureBox_DiceOne->Location = System::Drawing::Point(927, 452);
			this->pictureBox_DiceOne->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox_DiceOne->Name = L"pictureBox_DiceOne";
			this->pictureBox_DiceOne->Size = System::Drawing::Size(120, 114);
			this->pictureBox_DiceOne->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox_DiceOne->TabIndex = 21;
			this->pictureBox_DiceOne->TabStop = false;
			this->pictureBox_DiceOne->Visible = false;
			// 
			// pictureBox7
			// 
			this->pictureBox7->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox7.Image")));
			this->pictureBox7->Location = System::Drawing::Point(116, 303);
			this->pictureBox7->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox7->Name = L"pictureBox7";
			this->pictureBox7->Size = System::Drawing::Size(208, 144);
			this->pictureBox7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox7->TabIndex = 22;
			this->pictureBox7->TabStop = false;
			this->pictureBox7->Visible = false;
			// 
			// pictureBox8
			// 
			this->pictureBox8->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox8.Image")));
			this->pictureBox8->Location = System::Drawing::Point(485, 303);
			this->pictureBox8->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox8->Name = L"pictureBox8";
			this->pictureBox8->Size = System::Drawing::Size(207, 144);
			this->pictureBox8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox8->TabIndex = 23;
			this->pictureBox8->TabStop = false;
			this->pictureBox8->Visible = false;
			// 
			// pictureBox9
			// 
			this->pictureBox9->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox9.Image")));
			this->pictureBox9->Location = System::Drawing::Point(101, 87);
			this->pictureBox9->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox9->Name = L"pictureBox9";
			this->pictureBox9->Size = System::Drawing::Size(117, 78);
			this->pictureBox9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox9->TabIndex = 24;
			this->pictureBox9->TabStop = false;
			this->pictureBox9->Visible = false;
			// 
			// pictureBox10
			// 
			this->pictureBox10->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox10.Image")));
			this->pictureBox10->Location = System::Drawing::Point(599, 87);
			this->pictureBox10->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox10->Name = L"pictureBox10";
			this->pictureBox10->Size = System::Drawing::Size(115, 78);
			this->pictureBox10->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox10->TabIndex = 25;
			this->pictureBox10->TabStop = false;
			this->pictureBox10->Visible = false;
			// 
			// button2
			// 
			this->button2->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->button2->Location = System::Drawing::Point(523, 517);
			this->button2->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(100, 28);
			this->button2->TabIndex = 26;
			this->button2->Text = L"Exit";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Visible = false;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(173, 517);
			this->button3->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(100, 28);
			this->button3->TabIndex = 27;
			this->button3->Text = L"New Game";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Visible = false;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// shapeContainer1
			// 
			this->shapeContainer1->Location = System::Drawing::Point(0, 0);
			this->shapeContainer1->Margin = System::Windows::Forms::Padding(0);
			this->shapeContainer1->Name = L"shapeContainer1";
			this->shapeContainer1->Shapes->AddRange(gcnew cli::array< Microsoft::VisualBasic::PowerPacks::Shape^  >(1) {this->rectangleShape1});
			this->shapeContainer1->Size = System::Drawing::Size(1157, 741);
			this->shapeContainer1->TabIndex = 28;
			this->shapeContainer1->TabStop = false;
			// 
			// rectangleShape1
			// 
			this->rectangleShape1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)), 
				static_cast<System::Int32>(static_cast<System::Byte>(128)));
			this->rectangleShape1->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->rectangleShape1->BackStyle = Microsoft::VisualBasic::PowerPacks::BackStyle::Opaque;
			this->rectangleShape1->BorderStyle = System::Drawing::Drawing2D::DashStyle::Dot;
			this->rectangleShape1->CornerRadius = 5;
			this->rectangleShape1->FillColor = System::Drawing::Color::Lime;
			this->rectangleShape1->FillGradientColor = System::Drawing::Color::White;
			this->rectangleShape1->FillGradientStyle = Microsoft::VisualBasic::PowerPacks::FillGradientStyle::ForwardDiagonal;
			this->rectangleShape1->FillStyle = Microsoft::VisualBasic::PowerPacks::FillStyle::Percent05;
			this->rectangleShape1->Location = System::Drawing::Point(0, 2);
			this->rectangleShape1->Name = L"rectangleShape1";
			this->rectangleShape1->Size = System::Drawing::Size(833, 680);
			// 
			// timer1
			// 
			this->timer1->Interval = 200;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// pictureBox11
			// 
			this->pictureBox11->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->pictureBox11->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox11.Image")));
			this->pictureBox11->Location = System::Drawing::Point(927, 449);
			this->pictureBox11->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->pictureBox11->Name = L"pictureBox11";
			this->pictureBox11->Size = System::Drawing::Size(120, 114);
			this->pictureBox11->TabIndex = 29;
			this->pictureBox11->TabStop = false;
			// 
			// richTextBox1
			// 
			this->richTextBox1->BackColor = System::Drawing::Color::Black;
			this->richTextBox1->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->richTextBox1->ForeColor = System::Drawing::Color::White;
			this->richTextBox1->HideSelection = false;
			this->richTextBox1->Location = System::Drawing::Point(829, 289);
			this->richTextBox1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->ReadOnly = true;
			this->richTextBox1->Size = System::Drawing::Size(277, 152);
			this->richTextBox1->TabIndex = 30;
			this->richTextBox1->Text = L"1- Player One Click On Button\n     To  Roll Dice .\n2- Player Two Press D To\n     " 
				L"Roll Dice .\n3   Ctrl + N  For New Game.";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->label9->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 20.25F, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->label9->Location = System::Drawing::Point(832, 239);
			this->label9->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(247, 41);
			this->label9->TabIndex = 31;
			this->label9->Text = L"Game Instruction";
			this->label9->UseWaitCursor = true;
			// 
			// menuStrip1
			// 
			this->menuStrip1->AllowMerge = false;
			this->menuStrip1->AutoSize = false;
			this->menuStrip1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(255)), 
				static_cast<System::Int32>(static_cast<System::Byte>(255)));
			this->menuStrip1->Dock = System::Windows::Forms::DockStyle::Right;
			this->menuStrip1->Font = (gcnew System::Drawing::Font(L"Times New Roman", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(1), true));
			this->menuStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Visible;
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->singlePlayerToolStripMenuItem, 
				this->doublePlayerToolStripMenuItem, this->exitToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(1112, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Padding = System::Windows::Forms::Padding(4, 2, 0, 2);
			this->menuStrip1->Size = System::Drawing::Size(45, 741);
			this->menuStrip1->TabIndex = 32;
			this->menuStrip1->Text = L"menuStrip1";
			this->menuStrip1->TextDirection = System::Windows::Forms::ToolStripTextDirection::Vertical90;
			// 
			// singlePlayerToolStripMenuItem
			// 
			this->singlePlayerToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->singlePlayerToolStripMenuItem1, 
				this->doublePlayerToolStripMenuItem1});
			this->singlePlayerToolStripMenuItem->Name = L"singlePlayerToolStripMenuItem";
			this->singlePlayerToolStripMenuItem->Size = System::Drawing::Size(40, 103);
			this->singlePlayerToolStripMenuItem->Text = L"New Game";
			// 
			// singlePlayerToolStripMenuItem1
			// 
			this->singlePlayerToolStripMenuItem1->Name = L"singlePlayerToolStripMenuItem1";
			this->singlePlayerToolStripMenuItem1->ShowShortcutKeys = false;
			this->singlePlayerToolStripMenuItem1->Size = System::Drawing::Size(256, 26);
			this->singlePlayerToolStripMenuItem1->Text = L"Single Player    Ctrl +R";
			this->singlePlayerToolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::singlePlayerToolStripMenuItem1_Click);
			// 
			// doublePlayerToolStripMenuItem1
			// 
			this->doublePlayerToolStripMenuItem1->Name = L"doublePlayerToolStripMenuItem1";
			this->doublePlayerToolStripMenuItem1->ShowShortcutKeys = false;
			this->doublePlayerToolStripMenuItem1->Size = System::Drawing::Size(256, 26);
			this->doublePlayerToolStripMenuItem1->Text = L"Double Player  Ctrl +N";
			this->doublePlayerToolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::doublePlayerToolStripMenuItem1_Click);
			// 
			// doublePlayerToolStripMenuItem
			// 
			this->doublePlayerToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->singlePlayerToolStripMenuItem2, 
				this->doublePlayerToolStripMenuItem2});
			this->doublePlayerToolStripMenuItem->Name = L"doublePlayerToolStripMenuItem";
			this->doublePlayerToolStripMenuItem->Size = System::Drawing::Size(40, 65);
			this->doublePlayerToolStripMenuItem->Text = L"Player";
			// 
			// singlePlayerToolStripMenuItem2
			// 
			this->singlePlayerToolStripMenuItem2->Name = L"singlePlayerToolStripMenuItem2";
			this->singlePlayerToolStripMenuItem2->Size = System::Drawing::Size(194, 26);
			this->singlePlayerToolStripMenuItem2->Text = L"Single Player";
			this->singlePlayerToolStripMenuItem2->Click += gcnew System::EventHandler(this, &Form1::singlePlayerToolStripMenuItem2_Click);
			// 
			// doublePlayerToolStripMenuItem2
			// 
			this->doublePlayerToolStripMenuItem2->Name = L"doublePlayerToolStripMenuItem2";
			this->doublePlayerToolStripMenuItem2->Size = System::Drawing::Size(194, 26);
			this->doublePlayerToolStripMenuItem2->Text = L"Double Player";
			this->doublePlayerToolStripMenuItem2->Click += gcnew System::EventHandler(this, &Form1::doublePlayerToolStripMenuItem2_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(40, 46);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem_Click);
			// 
			// richTextBox2
			// 
			this->richTextBox2->BackColor = System::Drawing::Color::Black;
			this->richTextBox2->Font = (gcnew System::Drawing::Font(L"Times New Roman", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->richTextBox2->ForeColor = System::Drawing::Color::White;
			this->richTextBox2->HideSelection = false;
			this->richTextBox2->Location = System::Drawing::Point(829, 287);
			this->richTextBox2->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->richTextBox2->Name = L"richTextBox2";
			this->richTextBox2->ReadOnly = true;
			this->richTextBox2->Size = System::Drawing::Size(277, 154);
			this->richTextBox2->TabIndex = 33;
			this->richTextBox2->Text = L"1- Player One Click On Button\n     To  Roll Dice .\n2- Player Two is Computer\n3- C" 
				L"trl +R For New Game.";
			// 
			// button4
			// 
			this->button4->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 14.25F, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->button4->Location = System::Drawing::Point(861, 597);
			this->button4->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(201, 86);
			this->button4->TabIndex = 34;
			this->button4->Text = L"Computer Rolling Dice";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Visible = false;
			// 
			// button5
			// 
			this->button5->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 14.25F, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->button5->Location = System::Drawing::Point(861, 599);
			this->button5->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(201, 86);
			this->button5->TabIndex = 35;
			this->button5->Text = L"Player Two Rolling Dice";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Visible = false;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Font = (gcnew System::Drawing::Font(L"Lucida Calligraphy", 27.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label10->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"label10.Image")));
			this->label10->Location = System::Drawing::Point(83, 4);
			this->label10->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(665, 60);
			this->label10->TabIndex = 36;
			this->label10->Text = L"Snake And Ladder Game";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->BackColor = System::Drawing::Color::Black;
			this->label11->Font = (gcnew System::Drawing::Font(L"Times New Roman", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label11->ForeColor = System::Drawing::Color::White;
			this->label11->Location = System::Drawing::Point(843, 396);
			this->label11->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(253, 31);
			this->label11->TabIndex = 37;
			this->label11->Text = L"Computer Vs Player";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->BackColor = System::Drawing::Color::Black;
			this->label12->Font = (gcnew System::Drawing::Font(L"Times New Roman", 17.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label12->ForeColor = System::Drawing::Color::White;
			this->label12->Location = System::Drawing::Point(835, 399);
			this->label12->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(247, 32);
			this->label12->TabIndex = 38;
			this->label12->Text = L"Player1 Vs Player2";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(120, 120);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSize = true;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->BackColor = System::Drawing::SystemColors::Control;
			this->CancelButton = this->button2;
			this->ClientSize = System::Drawing::Size(1157, 741);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->pictureBox_DiceOne);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->pictureBox_DiceTwo);
			this->Controls->Add(this->pictureBox_DiceThree);
			this->Controls->Add(this->pictureBox_DiceFour);
			this->Controls->Add(this->pictureBox_DiceFive);
			this->Controls->Add(this->pictureBox_DiceSix);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->pictureBox10);
			this->Controls->Add(this->pictureBox9);
			this->Controls->Add(this->pictureBox8);
			this->Controls->Add(this->pictureBox7);
			this->Controls->Add(this->pictureBox6);
			this->Controls->Add(this->pictureBox5);
			this->Controls->Add(this->pictureBox4);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox3);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->pictureBox11);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->richTextBox1);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->richTextBox2);
			this->Controls->Add(this->shapeContainer1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->KeyPreview = true;
			this->MainMenuStrip = this->menuStrip1;
			this->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->MaximizeBox = false;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Snake Ladder";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Form1_KeyPress);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceSix))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceFive))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceFour))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceThree))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceTwo))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox_DiceOne))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox10))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox11))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		
		// SinglePlayer(!Single);
	void SinglePlayer(bool state)
	{
		timer1->Enabled=state;
	}
	void switchUser(void)
	{
     if (u==1)
	 {
		button1->Visible = false;
		SinglePlayer(Single);
		 u=2;
		 if (doubleplayer)
			{
				button4->Visible = false;
				button5->Visible = true;
		    }
		 else
		 {
			 button4->Visible = true;
			 button5->Visible = false;
		 }
	 }
     else
     {
         u=1;
		 button1->Visible = true;
		 button4->Visible = false;
	     button5->Visible = false;
	 }
	 label2->Text=System::Convert::ToString(u);
   }
	void  uesrFunction(int dice, int user)
	{
	int start=0;
	int pos=0;
	
	if (user==1)
	{
		start=u1Sart;
		pos=u1Pos;
	    
	}
	else
	{
		start=u2Start;
		pos=u2Pos;
	}
	if (start==0)
     {
		 if(user==1)
             {      if (dice==6)
				   {       
					   start=1;
					   pictureBox2->Visible=true;
					   pictureBox2->Location=Point(34,600);
				   }
				   else if (dice==12)
			       {
					   start=1;
					   pictureBox2->Visible=true;
					   pictureBox2->Location=Point(34,600);
					   pos=6;
		           }
			} 
		 else
			 {		if (dice==6)
				   {       
					   start=1;
					   pictureBox3->Visible=true;
					   pictureBox3->Location=Point(34,600);
				   }
			       else if (dice==12)
			       {
				       start=1;
					   pictureBox3->Visible=true;
					   pictureBox3->Location=Point(34,600);
					   pos=6;
		           }
			}
     }
     else
     {
					 if((pos+dice)>101 )
					{ 
								dice=0;
								pos+=dice;
					}		 
					else
							   (pos+=dice);
                                                    
          switch(pos)
          {
                      case 8:
						  ladderSound();
						  pos=31;
						  play_music();												//if (*pos==8)
                            break;
                      case 15: 
						  ladderSound();
						  pos=97;
						  play_music();												//if (*pos==15)
                            break;
                      case 24: 
							SnakeSound();
							pos=1;
							play_music();										// if (*pos==24)
                            break;
                      case 42: 
						  ladderSound();
						  pos=81;
						  play_music();											// if (*pos==42)
                            break;
                      case 55:
						  SnakeSound();											//if (*pos==55)   
                             pos=13;
						  play_music();
                              break;
                      case 66: 
						  ladderSound();										//if (*pos==66)
                             pos=87;
						  play_music();
                              break; 
                      case 71: 
						  SnakeSound();											//if (*pos==71)
                             pos=29;
						  play_music();
                              break;
                      case 88:
						  SnakeSound();										//if (*pos==88)
                             pos=67;
						  play_music();
                              break;
                      case 99: 
						  SnakeSound();											//if (*pos==99)
                             pos=6;
						  play_music();
                              break;
                      case 100:                                                    //if (*pos>=100)
                              cout<<"win "<<endl;
                              break;           
          }                                                
                                                                 
     }           
	 if (user==1)
	{
		u1Sart=start;
		u1Pos=pos;
	}
	else
	{
		u2Start=start;
		u2Pos=pos;
	}
                             
	}
	Point setLoction(int pos)
	{

	int Sx=34,Sy=600, xl=560;
	int c_x,c_y;
	int gw=58;

	if (pos<=10)
	{
		if(pos==10)
			c_x=pos-10;
		else
		c_x=pos-1;
		c_y=0;
	}
	else
	{	if(((pos)%(10))==0)
	{
		c_x=((pos)%(10));
		
	}	
		else
			 c_x=((pos-1)%(10));
	}
	c_y=((pos)/(10));
	if (c_y%2==0)
		x=((Sx)+((c_x)*(gw)));
		else
			x=(xl)-((c_x)*(gw));
	if(((pos)%(10))==0)
		y=(Sy)-((c_y-1)*(gw));
	else
		y=(Sy)-((c_y)*(gw));
		 
	return Point(x,y);
	}
	void DiscRoll()
	{
		srand((unsigned) time(0));
			 dice=(rand()%6)+1;
			 int n_Six=0;
			 switch(dice)
			 {
			 case 1:
					
					label1->Visible=false;
					pictureBox_DiceOne->Visible=true;
					pictureBox_DiceTwo->Visible=false;
					pictureBox_DiceThree->Visible=false;
					pictureBox_DiceFour->Visible=false;
					pictureBox_DiceFive->Visible=false;
					pictureBox_DiceSix->Visible=false;
					PlaySound(TEXT("one.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
					play_music();
					break;
			 case 2:
					
					label1->Visible=false;
					pictureBox_DiceOne->Visible=false;
					pictureBox_DiceTwo->Visible=true;
					pictureBox_DiceThree->Visible=false;
					pictureBox_DiceFour->Visible=false;
					pictureBox_DiceFive->Visible=false;
					pictureBox_DiceSix->Visible=false;
					PlaySound(TEXT("two.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
					play_music();
					pictureBox11->Visible=true;
					break;
			 case 3:
					
					label1->Visible=false;
					pictureBox_DiceOne->Visible=false;
					pictureBox_DiceTwo->Visible=false;
					pictureBox_DiceThree->Visible=true;
					pictureBox_DiceFour->Visible=false;
					pictureBox_DiceFive->Visible=false;
					pictureBox_DiceSix->Visible=false;
					PlaySound(TEXT("three.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
					play_music();
					break;
			 case 4:
					
					label1->Visible=false;
					pictureBox_DiceOne->Visible=false;
					pictureBox_DiceTwo->Visible=false;
					pictureBox_DiceThree->Visible=false;
					pictureBox_DiceFour->Visible=true;
					pictureBox_DiceFive->Visible=false;
					pictureBox_DiceSix->Visible=false;
					PlaySound(TEXT("four.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
					play_music();
					break;
			 case 5:
					
					label1->Visible=false;
					pictureBox_DiceOne->Visible=false;
					pictureBox_DiceTwo->Visible=false;
					pictureBox_DiceThree->Visible=false;
					pictureBox_DiceFour->Visible=false;
					pictureBox_DiceFive->Visible=true;
					pictureBox_DiceSix->Visible=false;
					PlaySound(TEXT("five.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
					play_music();
					break;
			 case 6:
					label1->Visible=false;
					pictureBox_DiceOne->Visible=false;
					pictureBox_DiceTwo->Visible=false;
					pictureBox_DiceThree->Visible=false;
					pictureBox_DiceFour->Visible=false;
					pictureBox_DiceFive->Visible=false;
					pictureBox_DiceSix->Visible=true;
					PlaySound(TEXT("six.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
					play_music();
					break;
			 default :
					pictureBox11->Visible=true;
			 }
			  
			 if (u==1)
             {
				
                if (dice==6)
                   { 
					   
                     
					  sixCount++;
					  if(u1Pos+(6*sixCount)+dice>101)
					  {
							  if (sixCount==1)
							  {
								  sixCount=0;
								  switchUser();
							  }
						      return;
					  }
					  else
					  {
							  if (sixCount==3)
							  {
								  sixCount=0;
								  switchUser();
							  } 
							  return;
							  
					  }
					  
					  
                   }
                		uesrFunction((6*sixCount), u);
						uesrFunction(dice, u);
						pictureBox2->Location=setLoction(u1Pos);
						sixCount=0;
						n_Six=0;
						switchUser();
						
             }
             else
             {
				 
                 if (dice==6)
                   {
					   sixCount++;
					  if ((u2Pos+(6*sixCount)+dice)>101)
					  {   sixCount=0;
						  switchUser();
						   
					  }
					  else
					  {
                      
                      if (sixCount==3)
                      {
                          sixCount=0;
                          switchUser();
                      }  
					  
					  return;
					   }
					    
                   }
                 	    uesrFunction((6*sixCount), u);
						uesrFunction(dice, u);
						pictureBox3->Location=setLoction(u2Pos);
						n_Six=0;
						sixCount=0;
						switchUser();
				 
               
             }	
			label4->Text=System::Convert::ToString(u1Pos);
			label5->Text=System::Convert::ToString(u2Pos);
			 if((u1Pos>100) || (u2Pos>100))
             {
				 SinglePlayer(!Single);
				 pictureBox7->Visible=true;
				 pictureBox8->Visible=true;
				 pictureBox9->Visible=true;
				 pictureBox10->Visible=true;
				 button2->Visible=true;
				 button3->Visible=true;
				 label2->Visible=false;
				 label3->Visible=false;
				 button1->Visible=false;
				  label1->Visible=false;
				 play_music();
				 if(u1Pos>100)
				 { 
					 pictureBox6->Visible=true;
					 pictureBox2->Visible=false;
					 pictureBox3->Visible=false;
					 return;
				 }
				if(u2Pos>100)
				 {
					 pictureBox5->Visible=true;
					 pictureBox3->Visible=false;
					 pictureBox2->Visible=false;
					 return;
				 }
				
                    return;  
					
             }
			 }
	
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				 DiscRoll();
		  }
	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			Form1();
		 }
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
					Application::Exit();
					
		 }
void SnakeSound()
{
	PlaySound(TEXT("EatingSnak.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
}
 void NewGame()
 {
	     	dice=0;
			u1Pos=0, u2Pos=0;
			u1Sart=0;
			u2Start=0;
			u=1;
			sixCount=0;
			key=0;
			x=pictureBox2->Location.X;
			y=pictureBox2->Location.Y;
			g=0;
			button1->Visible = false;
			doubleplayer=false;
			Single=false;
	        x=32;
			y=600;
			button1->Visible=false;
			button2->Visible=false;
			button3->Visible=false;
				pictureBox7->Visible=false;
				pictureBox8->Visible=false;
				pictureBox9->Visible=false;
				pictureBox10->Visible=false;
			pictureBox6->Visible=false;
		    pictureBox5->Visible=false;
				pictureBox3->Visible=false;
				pictureBox2->Visible=false;
			label2->Visible=true;
			label3->Visible=true;
			label1->Visible=true;
					pictureBox_DiceOne->Visible=false;
					pictureBox_DiceTwo->Visible=false;
					pictureBox_DiceThree->Visible=false;
					pictureBox_DiceFour->Visible=false;
					pictureBox_DiceFive->Visible=false;
					pictureBox_DiceSix->Visible=false;
					label4->Text=System::Convert::ToString(0);
			        label5->Text=System::Convert::ToString(0);
			
 }
void play_music()
{
		PlaySound(TEXT("lastBgM.wav"),NULL, SND_NOSTOP | SND_LOOP | SND_ASYNC  );
}
void ladderSound()
{
	PlaySound(TEXT("Wak.wav"),NULL, SND_LOOP | SND_SYNC|SND_FILENAME);
    }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

			play_music();
			
	}
private: System::Void Form1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			if (doubleplayer)
			{
			 if (e->KeyChar == 14) 
				 {
					 NewGame();
					 button1->Visible =true;
					 richTextBox1->Visible= true;
			         richTextBox2->Visible= false;
					 button4->Visible =false;
					 button5->Visible =false;
					 label12->Visible= true;
					 label11->Visible= false;
				 }
			 else if (e->KeyChar==100)
			 {
				 if (button1->Visible == false)
			 		 DiscRoll();
				else
				     MessageBox::Show("Please Wait For Your Turn");
			 }
			 else
			 MessageBox::Show("Please Enter Valid Key");
			}
			else
			{
				if (e->KeyChar == 18) 
				  {
					  NewGame();
					  richTextBox1->Visible= false;
			          richTextBox2->Visible= true;
					  button4->Visible =false;
					  button5->Visible =false;
					  label12->Visible= false;
					  label11->Visible= true;
				 }
			}
		 }

private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 g++;
			 if(!doubleplayer)
			 {
				 if (g>10)
				{
					DiscRoll();
					g=0;
				}
			 }
		 }

private: System::Void doublePlayerToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
			 NewGame();
			 doubleplayer=true;
			 button1->Visible = true;
			 richTextBox1->Visible= true;
			 richTextBox2->Visible= false;
			 button4->Visible =false;
			 button5->Visible =false;
			 label12->Visible= true;
			 label11->Visible= false;

		}
private: System::Void singlePlayerToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
			 NewGame();
			 u1Pos=0;
			 u2Pos=0;
			 u1Sart=0;
			 u2Start=0;
			 Single= true;
			 button1->Visible = true;
			 richTextBox1->Visible= false;
			 richTextBox2->Visible= true;
			 button4->Visible =false;
			 button5->Visible =false;
			 label12->Visible= false;
			 label11->Visible= true;
		}
private: System::Void singlePlayerToolStripMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {
			 doubleplayer=false;
			 button1->Visible = true;
			 Single= true;
			 richTextBox1->Visible= false;
			 richTextBox2->Visible= true;
			 label12->Visible= false;
			 label11->Visible= true;
		 }
private: System::Void doublePlayerToolStripMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {
			 doubleplayer=true;
			 button1->Visible = true;
			 Single= false;
			 richTextBox1->Visible= true;
			 richTextBox2->Visible= false;
			 label12->Visible= true;
			 label11->Visible= false;
		 }
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 Application::Exit();
		 }
};
}

